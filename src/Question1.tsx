import React, { useEffect, useState } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Caption, useTheme, Title, Button } from 'react-native-paper'
import FacebookLogin from './FacebookLogin';

export default function Question1({ route, navigation, props }) {
  const [ userData, setUserData ] = useState({});
  const { colors: { background } } = useTheme();

  useEffect(() => {
    if (userData['register']) {
      navigation.navigate('question2', { 
        userData: {
          register: true,
          id: userData['id'],
          name: userData['name'],
          email: userData['email'],
          profile_img: userData['picture']['data']['url']
        } 
      })
    }
    if (userData['exists']) {
      navigation.navigate('accountDashboard', { accountData: userData })
    }
  })

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:50}}>Identifique-se para continuar</Title>
      </View>
      <View style={styles.checkboxContainer}>
        <FacebookLogin {...props} setUserData={setUserData} />
        <Button 
          {...props} 
          icon="google" 
          onPress={() => navigation.navigate('question3')}
          style={{marginTop:10, marginBottom:10, padding:30}} 
          mode="contained"
        >
          Entrar com Google
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});
