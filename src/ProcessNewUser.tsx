import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Paragraph, ActivityIndicator, Caption, useTheme, Title } from 'react-native-paper'

export default function ProcessNewUser({ route, navigation, props }) {
  const userData = route.params.userData
  const valorHora = route.params.valorHora
  // const enderecoNumero = route.params.enderecoNumero
  // const address = route.params.address
  // const selectedTipoDeDeslocamento = route.params.selectedTipoDeDeslocamento
  const CEP = route.params.CEP
  const { colors: { background } } = useTheme()

  const accountData = {
    Name: userData.name,
    Social_Media_ID__c: userData.id,
    Email__c: userData.email || '',
    Photo_URL__c: userData.profile_img,
    Valor_Hora__c: valorHora,
    Saldo__c: 0,
    Score__c: 0,
    Disponivel__c: true,
    Reputacao__c: 0,
    Primeiro_Trabalho__c: true,
    CEP__c: CEP,
    // Endereco__c: address,
    // Regiao__c: 'São Paulo', //@todo
    // Endereco_Numero__c: enderecoNumero,
    Auth_Type__c: 'Facebook', //@todo
    // Tipo_Deslocamento__c: selectedTipoDeDeslocamento,
    register: true,
    ExponentPushToken__c: null,
    Proxima_Transferencia__c: null,
    Trabalhos: []
  }

  if (route.params.processGo) {
    fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasInterpreteAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(accountData)
    }).then(response => response.json()).then(sfId => {
      accountData['sfId'] = sfId
      navigation.navigate('accountDashboard', { accountData })
    }).catch((error) => {
      console.error(error);
    });
  }

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:10}}>Olá, {userData.name.split(' ')[0]}!</Title>
      </View>
      <View>
        <Paragraph style={{textAlign:'center',fontStyle:'italic', marginBottom:80}}>Carregando Dashboard...</Paragraph>
        <ActivityIndicator {...props} animating={true} size="large" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    checkboxContainer: {
      flexDirection: "row",
      marginBottom: 10,
    },
    checkbox: {
      alignSelf: "center",
    },
    label: {
      margin: 8,
    },
  });