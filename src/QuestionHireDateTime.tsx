import React, { useState } from 'react';
import { View, StyleSheet, Image, Platform } from 'react-native';
import { Caption, useTheme, Title, Button, Text } from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';

export default function QuestionHireDateTime({ route, navigation, props }) {
  const newContratante  = route.params.newContratante
  const { colors: { background } } = useTheme()
  const [timeStart, setTimeStart] = useState(new Date());
  const [timeEnd, setTimeEnd] = useState(new Date());
  const [date, setDate] = useState(new Date());
  const [showDate, setShowDate] = useState(false);
  const [showTimeStart, setShowTimeStart] = useState(false);
  const [showTimeEnd, setShowTimeEnd] = useState(false);
  const [dateInteraction, setDateInteraction] = useState(false)
  const [timeStartInteraction, setTimeStartInteraction] = useState(false)
  const [timeEndInteraction, setTimeEndInteraction] = useState(false)

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDate(Platform.OS === 'ios');
    setDate(currentDate);
    setDateInteraction(true)
  };

  const onChangeTimeStart = (event, selectedTimeStart) => {
    const currentTimeStart = selectedTimeStart || timeStart;
    setShowTimeStart(Platform.OS === 'ios');
    setTimeStart(currentTimeStart);
    setTimeStartInteraction(true)
  };

  const onChangeTimeEnd = (event, selectedTimeEnd) => {
    const currentTimeEnd = selectedTimeEnd || timeEnd;
    setShowTimeEnd(Platform.OS === 'ios');
    setTimeEnd(currentTimeEnd);
    setTimeEndInteraction(true)
  };

  const showDatepicker = () => {
    setShowDate(true);
  };

  const showTimeStartpicker = () => {
    setShowTimeStart(true);
  };
  const showTimeEndpicker = () => {
    setShowTimeEnd(true);
  };

  const parseDate = (date) => {
    if (!date) return
    var splitDate = date.toISOString().split('T')[0]
    splitDate = splitDate.split('-')
    return splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]
  }

  const parseTime = (time) => {
    if (!time) return
    var splitTime = time.toISOString().split('T')[1]
    splitTime = splitTime.split(':')
    return splitTime[0] + ':' + splitTime[1]
  }

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:40}}>Selecione a data e horário para a realização do serviço</Title>
      </View>

      <View style={styles.checkboxContainer}>
        {Platform.OS === 'ios' ? (
          <View style={{flex:1, paddingLeft:20, paddingBottom:10}}>
            <Text {...props}>DIA:</Text>
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode="date"
            display="compact"
            onChange={onChangeDate}
          />
        
        </View>
          
        ) : (
          <Button
            {...props}
            label="Data"
            mode="outlined"
            onPress={showDatepicker}
            style={{width:340, padding:10}}
          >DIA<Text {...props} style={{color:'#000'}}>{dateInteraction ? ': ' + parseDate(date) : ''}</Text></Button>
        )}
        
      </View>
      <View style={styles.checkboxContainer}>
      {Platform.OS === 'ios' ? (
        <View style={{flex:1, paddingLeft:20, paddingBottom:10}}>
        <Text {...props}>HORÁRIO INÍCIO:</Text>
        <DateTimePicker
        testID="dateTimePickerTimeStart"
        value={timeStart}
        mode="time"
        display="compact"
        onChange={onChangeTimeStart}
      />
      
      </View>
      ) : (
        <Button
          {...props}
          onPress={showTimeStartpicker}
          mode="outlined"
          style={{width:340, padding:10}}
        >Horário Início<Text {...props}
         style={{color:'#000'}}>{timeStartInteraction ? ': ' + parseTime(timeStart) : ''}</Text></Button>
      )}
        
      </View>
      <View style={styles.checkboxContainer}>
        {Platform.OS === 'ios' ? (
        <View style={{flex:1, paddingLeft:20, paddingBottom:10}}>
            <Text {...props}>HORÁRIO TÉRMINO:</Text>
          <DateTimePicker
          testID="dateTimePickerTimeEnd"
          value={timeEnd}
          mode="time"
          display="compact"
          onChange={onChangeTimeEnd}
        />
        
      </View>
        
        ) : (
          
        <Button
        {...props}
        onPress={showTimeEndpicker}
        mode="outlined"
        style={{width:340, padding:10}}
      >Horário Término<Text {...props}
       style={{color:'#000'}}>{timeEndInteraction ? ': ' + parseTime(timeEnd) : ''}</Text></Button>
        )}
      </View>

      {Platform.OS !== 'ios' && (
        <View>
          {showDate && (
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode="date"
              is24Hour={true}
              display="default"
              onChange={onChangeDate}
            />
          )}
          {showTimeStart && (
            <DateTimePicker
              testID="dateTimePickerTimeStart"
              value={timeStart}
              mode="time"
              is24Hour={true}
              display="default"
              onChange={onChangeTimeStart}
            />
          )}
          {showTimeEnd && (
            <DateTimePicker
              testID="dateTimePickerTimeEnd"
              value={timeEnd}
              mode="time"
              is24Hour={true}
              display="default"
              onChange={onChangeTimeEnd}
            />
          )}
        </View>
      )}

      {/* <Button {...props} icon="plus" mode="contained" onPress={() => console.log('Pressed')}>
        Adicionar data
      </Button> */}

      <View>
        <View style={{marginTop:50}}>
          <Button 
            {...props}
            disabled={!dateInteraction || !timeStartInteraction || !timeEndInteraction}
            style={{
              marginTop:10, 
              marginBottom:10, 
              paddingLeft:50,
              paddingRight:50,
              paddingTop:20,
              paddingBottom:20,
            }} 
            onPress={() => navigation.navigate('questionHireLocal', { newContratante, hireDateTimeConfig: { date: parseDate(date), timeStart: parseTime(timeStart), timeEnd: parseTime(timeEnd) } })} 
            labelStyle={{fontSize:16}}  
            mode="contained"
          >
            Continuar
          </Button>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});