import React, { useState } from 'react'
import { View, Image, ScrollView, StyleSheet, Text } from 'react-native'
import { Divider, Button, Chip, List, useTheme } from 'react-native-paper'
import { CancelInvitedEvent } from './Examples/Dialogs';

export default function EventDetails({ route, navigation, props }) {
  const { colors } = useTheme();

  const monthMap = {
    '01': 'Janeiro',
    '02': 'Fevereiro',
    '03': 'Março',
    '04': 'Abril',
    '05': 'Maio',
    '06': 'Junho',
    '07': 'Julho',
    '08': 'Agosto',
    '09': 'Setembro',
    '10': 'Outubro',
    '11': 'Novembro',
    '12': 'Dezembro',
  }

  const [visible, setVisible] = useState({})
  const _toggleDialog = (name: string) => () => setVisible({ ...visible, [name]: !visible[name] });
  const _getVisible = (name: string) => !!visible[name];

  const servico = route.params.servico
  const trabalho = route.params.trabalho
  const userData = route.params.userData

  const postServiceAcceptance = (decision) => () => {
    fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasExponentPushTokenAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
        decision,
        trabalhoId: trabalho.Id, 
        servicoId: servico.Id, 
        contratanteId: trabalho.Contratante.Id 
      })
    }).then(resp => resp.json()).then(contratanteToken => {
      fetch('https://api.expo.dev/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify([{"to":contratanteToken,"title":trabalho.Name,"body":userData.Name + ' ' + (decision === 'accept' ? 'aceitou' : 'recusou') + ' o trabalho'}])
      })
    })
  }
  
  if (!servico || !trabalho) {
    navigation.navigate('accountDashboard')
  }
  
  return (
      <ScrollView style={{paddingTop:20}}>
        <View>
          <View style={{marginLeft:20, marginRight:20}}>
            <List.Section 
              {...props}
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}
            >
              <View>
                {servico.Status__c === 'Agendado' ? (
                  <Chip {...props} icon="clock" style={{backgroundColor:'#f1c40f'}}>
                    {servico.Status__c}
                  </Chip>
                ) : null}
                {servico.Status__c === 'Cancelado' ? (
                  <Chip {...props} icon="block-helper" style={{backgroundColor:'#e74c3c'}}>
                    {servico.Status__c}
                  </Chip>
                ) : null}
                {servico.Status__c === 'Iniciado' ? (
                  <Chip {...props} icon="check" style={{backgroundColor:'#3498db'}}>
                    {servico.Status__c}
                  </Chip>
                ) : null}
                {servico.Status__c === 'Finalizado' ? (
                  <Chip {...props} icon="check" style={{backgroundColor:'#2ecc71'}}>
                    {servico.Status__c}
                  </Chip>
                ) : null}
                {servico.Status__c === 'Aberto' ? (
                  <Chip {...props} icon="calendar-alert" style={{backgroundColor:'#e67e22'}}>
                    {servico.Status__c}
                  </Chip>
                ) : null}
              </View>
            </List.Section>
            <Text style={{fontSize:30, fontWeight:'bold'}}>{trabalho.Name}</Text>
            <View style={{flexDirection:'row'}}>
              <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="calendar" />
              <View>
                <Text style={{fontSize:15,marginTop:10,marginBottom:10}}>{servico.Data__c.split('-')[2]} de {monthMap[servico.Data__c.split('-')[1]]} de {servico.Data__c.split('-')[0]}</Text>
              </View>
            </View>
            <View style={{flexDirection:'row'}}>
              <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="clock" />
              <View>
                <Text style={{color:'#000',fontSize:15,marginTop:10,marginBottom:10}}>{servico.Hora_Inicio__c.split(':00.000Z')[0]} - {servico.Hora_Fim__c.split(':00.000Z')[0]}</Text>
              </View>
            </View>
            <View style={{flexDirection:'row'}}>
              <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="wallet" />
              <View>
                <Text style={{marginTop:10, marginBottom:10,fontSize:15,fontWeight:'bold', color:'darkgreen'}}>R$ {servico.Lucro_Interprete__c}</Text>
              </View>
            </View>
            {trabalho.Link__c && trabalho.Link__c !== '' ? (
              <View style={{flexDirection:'row'}}>
                <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="link" />
                <View>
                  <Text style={{fontSize:15,marginTop:10,marginBottom:10, color:'#000'}}>{trabalho.Link__c}</Text>
                </View>
              </View>
            ) : (
              <View style={{flexDirection:'row'}}>
                <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="map-marker" />
                <View>
                  <Text style={{fontSize:15,marginTop:10,marginBottom:10, color:'#000'}}>{trabalho.Local__c}</Text>
                </View>
              </View>
            )}
          </View>
        </View>
        
        <Divider {...props} style={{marginLeft:20, marginRight:20, marginTop:10}}/>
      
        <View style={{margin:20}}>
          <Text style={{fontSize:18,lineHeight:25}}>{trabalho.Contexto__c}</Text>
        </View>

        <Divider {...props} style={{marginLeft:20, marginRight:20}}/>
        
        <View style={{marginLeft:20, marginTop:20}}>
          <Text  style={{fontSize:17, fontWeight:'bold',}}>INTÉRPRETES</Text>
        </View>
        
        <List.Section
          {...props} 
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            paddingHorizontal: 12
          }}
        >
          {trabalho.interpretesTrabalho.map((interpreteTrabalho, index) => (
            <View key={index}>
              <Chip
                {...props}
                avatar={
                  <Image source={{uri: interpreteTrabalho.Photo_URL__c}} />
                }
              >
                {interpreteTrabalho.Name}
              </Chip>
            </View>  
          ))}
        </List.Section>
      
      <View style={{marginLeft:20, marginTop:20}}>
        <Text  style={{fontSize:17, fontWeight:'bold',}}>CONTRATANTE</Text>
      </View>
      
        <List.Section 
          {...props}
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            paddingHorizontal: 12
          }}
        >
          <View>
            <Chip
              {...props}
              avatar={
                <Image source={{uri: trabalho.Contratante.Photo_URL__c}} />
              }
            >
              {trabalho.Contratante.Name}
            </Chip>
          </View>
        </List.Section>
      
        <CancelInvitedEvent
          visible={_getVisible('cancelInvitedEvent')}
          close={_toggleDialog('cancelInvitedEvent')}
        />

<Divider {...props} style={{margin:10}}/>
      
<List.Section {...props}>
  {servico.Status__c === 'Aberto' ? (
    <View style={{...styles.row, justifyContent:'center'}}>
      <Button
        {...props}
        icon="check"
        mode="contained"
        color={colors.accent}
        onPress={postServiceAcceptance('accept')}
        style={{padding:20}}
      >
        ACEITAR TRABALHO
      </Button>
      <Button
        {...props}
        // mode="contained"
        icon="close"
        onPress={postServiceAcceptance('decline')}
        style={{padding:10}}
      >
        <Text style={{textDecorationLine:'underline'}}>RECUSAR</Text>
      </Button>
    </View>
  ) : (
    <View style={{...styles.row, justifyContent:'center'}}>
      <Button
        {...props}
        icon="chat"
        mode="contained"
        color={colors.accent}
        onPress={() => {}}
        style={{marginBottom:20, padding:20}}
      >
        CONVERSAR COM {trabalho.Contratante.Name.split(' ')[0]}
      </Button>
      <Button
        {...props}
        mode="contained"
        icon="trash-can"
        onPress={_toggleDialog('cancelInvitedEvent')}
        style={{marginBottom:50, padding:10}}
      >
        CANCELAR TRABALHO
      </Button>
    </View>
  )}
</List.Section>

</ScrollView>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:50,
    height:50
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 12,
  },
});