import * as React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Appbar } from 'react-native-paper';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import Welcome, { screens } from './Welcome';

const Stack = createStackNavigator();

export default function Root({ route, navigation, props }) {
  console.log('root')
  console.log(navigation)
  return (
    <Stack.Navigator
      headerMode="screen"
      screenOptions={{
        header: ({ navigation, scene, previous }) => (
          <View style={{
            height:['accountDashboard', 'eventDetails'].indexOf(scene.route.name) !== -1  ? 120 : 0, 
            marginTop:['accountDashboard', 'eventDetails'].indexOf(scene.route.name) !== -1  ? 0 : 60,
            paddingLeft:10,
            paddingRight:10
          }}>
            {['accountDashboard', 'eventDetails'].indexOf(scene.route.name) !== -1  ? (
              <View style={{paddingTop:50,position:'relative',flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                <View>
                  {previous && scene.route.name === 'eventDetails' ? (
                    <Appbar.BackAction {...props} size={45} onPress={() => navigation.goBack()} />
                  ) : (
                    <Appbar.Action
                    {...props}
                      icon="menu"
                      size={45}
                      onPress={() =>
                        ((navigation as any) as DrawerNavigationProp<{ navigation }>).openDrawer()
                      }
                      style={{margin:0, padding:0}}
                    />
                  )}
                </View>
                <View style={{marginBottom:50}}>
                  <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
                </View>
                <View>
                  <Appbar.Action
                    {...props}
                    icon="chat"
                    size={45}
                    onPress={() =>
                      ((navigation as any) as DrawerNavigationProp<{ navigation }>).openDrawer()
                    }
                  />
                </View>
              </View>
            ) : null}
          </View>
        )
      }}
    >
      <Stack.Screen
        {...props}
        name="welcome"
        component={Welcome}
        headerMode="none"
      />
      {(Object.keys(screens) as Array<keyof typeof screens>).map(id => (
        <Stack.Screen
          {...props}
          key={id}
          name={id}
          component={screens[id]}
          options={{ title: id }}
        />
      ))}
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  photo: {
    width:50,
    height:50
  },
});