import React, { useEffect, useState } from 'react'
import { ScrollView, Alert, Linking, View, StyleSheet, Image } from 'react-native'
import { List, Avatar, Divider, TextInput, ActivityIndicator, Text, Title, Caption, useTheme, Button, Paragraph } from 'react-native-paper'
import { PaymentReview } from './Examples/Dialogs';
import { WebView } from 'react-native-webview';


export default function ListaInterpretes({ route, navigation, props }) {
  const { colors: { background } } = useTheme();  
  const isReady = React.useRef(false);
  const newContratante = route.params.newContratante
  const local = route.params.local
  const hireDateTimeConfig = route.params.hireDateTimeConfig
  const qtyInterpretes = parseInt(route.params.qtyInterpretes) || 1
  const [ interpretes, setInterpretes ] = useState([])
  const [ isLoaded, setIsLoaded ] = useState(false)
  const [ selectedInterpretes, setSelectedInterpretes ] = useState([])
  // const [ ocasiao, setOcasiao ] = useState('');
  // const [ contexto, setContexto ] = useState('');
  // const [ pedidoId, setPedidoId ] = useState(null);
  const [ visible, setVisible ] = useState({});
  const [ goToPayment, setGoToPayment ] = useState(false)
  const [ isLoadingOrderConfirmation, setIsLoadingOrderConfirmation ] = useState(false)

  const [ paymentProcessed, setPaymentProcessed ] = useState(false)

  const getInterpretes = () => {
    fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasListaInterpretesAPI')
      .then(response => response.json()).then(responseRaw => {
        if (!isReady.current) {
          setInterpretes(JSON.parse(responseRaw))
          setIsLoaded(true)
        }
      }).catch((error) => {
        console.error(error);
      });
  }

  const toggleInterpreteSelection = (interprete) => {
    if (!isSelected(interprete.Id)) {
      setSelectedInterpretes(arr => [...arr, interprete])
    } else {
      setSelectedInterpretes(selectedInterpretes.filter(interpreteItem => interpreteItem.Id !== interprete.Id));
    }
  }

  useEffect(() => {
    getInterpretes()
    return () => {
      isReady.current = true
    }
  }, [])
  
  const isSelected = (interpreteId) => {
    if (!selectedInterpretes || !selectedInterpretes.length) return false
    let selected = false
    selectedInterpretes.map(selectedInterprete => {
      if (selectedInterprete.Id === interpreteId) {
        selected = true
      }
    })    
    return selected
  }
  
  const _getVisible = (name: string) => !!visible[name];

  const _toggleDialog = (name: string) => () => {
    return setVisible({ ...visible, [name]: !visible[name] })
  };

  const stateChange = (state) => {
    switch (state.title) {
      case 'success':
        setGoToPayment(false)
        setVisible(false)
        setPaymentProcessed(true)

        navigation.navigate('questionHireEventDetails', { 
          selectedInterpretes,
          local,
          hireDateTimeConfig, 
          newContratante, 
          paymentStatus: 'APROVADO',  
        })

      break;
      case 'pending':
        setGoToPayment(false)
        Alert.alert("Pagamento pendente!")
      break;
      case 'failure':
        setGoToPayment(false)
        Alert.alert("Pagamento não aprovado!")
      break;
    }
  }

  const placeOrder = () => {
    setIsLoadingOrderConfirmation(true)
    setGoToPayment(true)
    setIsLoadingOrderConfirmation(false)
  }

  if (goToPayment) {
    if (isLoadingOrderConfirmation) {
      return (<ActivityIndicator {...props} animating={true} size="large" />)
    } else {
      return (     
        <WebView
          source={{ uri: `http://192.168.0.21:3333/payments/checkout/pedidotest-${(+ new Date).toString()}/${'rodrigo.morais.ng@gmail.com'}/${'descricaoPagamento'}/300` }}
          onNavigationStateChange={state => stateChange(state)}
          startInLoadingState={true}
          renderLoading={() => <ActivityIndicator {...props}></ActivityIndicator>}
        />
      )
    }
  
  } else {
    if (!paymentProcessed) {
      return (
        <View style={styles.container}>
          <ScrollView>
          <View style={{marginBottom:20, alignItems:'center'}}>
            <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
            <View>
              <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
            </View>
          </View>
          <View >
            <Title style={{textAlign:'center', marginLeft:20, marginRight:20, marginBottom:40}}>INTÉRPRETES DE LIBRAS DISPONÍVEIS</Title>
          </View>
          <View style={{flexDirection:'row', flexWrap:'wrap'}}>
            { isLoaded && interpretes.map((interprete, index) => (
              <View key={index} style={{width:'50%', alignItems:'center', marginBottom:30}}>
                <Avatar.Image {...props} size={100} source={{uri: interprete.Photo_URL__c}} />
                {!interprete['Reputacao__c'] || interprete['Reputacao__c'] === 0 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {interprete['Reputacao__c'] === 1 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {interprete['Reputacao__c'] === 2 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {interprete['Reputacao__c'] === 3 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {interprete['Reputacao__c'] === 4 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {interprete['Reputacao__c'] === 5 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                <Text {...props} style={{fontWeight:'bold',fontSize:18}}>{interprete.Name}</Text>
                <View style={{flexDirection:'row'}}>
                  <List.Icon 
                    style={{
                      marginTop:10, 
                      marginBottom:10, 
                      marginLeft:0, 
                      height:25,
                      width:25, 
                      marginRight:5
                    }} 
                    icon="wallet" 
                  />
                  <View>
                    <Text 
                      {...props} 
                      style={{
                        marginTop:10, 
                        marginBottom:10,
                        fontSize:15,
                        fontWeight:'bold', 
                        color:'darkgreen'
                      }}
                    >R$ {interprete.Valor_Hora__c}/h</Text>
                  </View>
                </View>
                {isSelected(interprete.Id) ? (
                  <Button 
                    {...props}
                    icon="check"  
                    onPress={() => toggleInterpreteSelection(interprete)}
                    style={{marginBottom:10, padding:10}} 
                    labelStyle={{fontSize:13}}  
                    mode="contained"
                    color="lightgray"
                  >     
                    SELECIONADO
                  </Button>
                ) : (
                  <Button 
                    {...props}
                    icon="cart"  
                    onPress={() => toggleInterpreteSelection(interprete)}
                    style={{marginBottom:10, padding:10}} 
                    labelStyle={{fontSize:13}}  
                    mode="contained"
                  >     
                    SELECIONAR
                  </Button>
                )}
                
              </View>
            ))}
          </View>
        </ScrollView>
        
        <View>
          <Divider {...props}/>
          <Text style={{textAlign:'center', fontSize:16, margin:10}} {...props}>{(selectedInterpretes.length || 0) + ' de ' + qtyInterpretes + ' selecionados'}</Text>
          <Button 
            {...props}  
            disabled={selectedInterpretes.length !== qtyInterpretes}
            onPress={_toggleDialog('paymentReviewPopup')}
            style={{marginBottom:20, padding:15, paddingLeft:40, paddingRight:40}} 
            labelStyle={{fontSize:13}}  
            mode="contained"
          >     
            FINALIZAR
          </Button>
        </View>
          <PaymentReview
            {...props}
            props={props}
            setGoToPayment={setGoToPayment}
            selectedInterpretes={selectedInterpretes}
            visible={_getVisible('paymentReviewPopup')}
            close={_toggleDialog('paymentReviewPopup')}
            placeOrder={placeOrder}
          />
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <View style={{marginBottom:50}}>
            <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
            <View>
              <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
            </View>
          </View>          
          <View>
            <Paragraph style={{textAlign:'center'}}>Pagamento concluído!</Paragraph>
            <Text {...props} style={{textAlign:'center', fontStyle:'italic'}}>Carregando...</Text>
            <ActivityIndicator {...props} animating={true} size="large" />
          </View>
        </View>
      )
    }
  }
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});