import React, { useState } from 'react';
import { View, StyleSheet, Image, ImageBackground, CheckBox, Text } from 'react-native';
import { ToggleButton, Caption, List, useTheme, Title, Button, Checkbox } from 'react-native-paper';
import { StackNavigationProp } from '@react-navigation/stack';

type Props = {
  navigation: StackNavigationProp<{ [key: string]: undefined }>;
};

export default function QuestionHireType({ route, navigation, props }) {
  const { colors: { background } } = useTheme();
  const logged = route.params.logged

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
          <View>
            <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
          </View>
        </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:50}}>Como deseja contratar o Serviço de Intérprete de Libras?</Title>
      </View>
      <View style={styles.checkboxContainer}>
        <Button 
          {...props}
          icon="calendar-check" 
          onPress={() => navigation.navigate('questionHireDateTime', { newContratante: { type: 'hora'} })} 
          style={{marginTop:10, marginBottom:10, padding:30}} 
          labelStyle={{fontSize:13}}  
          mode="contained"
        >     
          Hora avulsa
        </Button>
        <Button 
          disabled={true}
          {...props}
          icon="calendar-week" 
          onPress={() => navigation.navigate('question2Hire', { newContratante: { type: 'assinatura'} })}
          style={{marginTop:10, marginBottom:10, paddingTop:30,paddingBottom:30,paddingLeft:10,paddingRight:10}} mode="contained"
        >
          Recorrente (Assinatura)
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});
