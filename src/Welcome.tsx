import * as React from 'react';
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native';
import { useTheme, Button, Caption } from 'react-native-paper';

import Home from './ApplicationHome';
import Question1 from './Question1';
import QuestionHireType from './QuestionHireType';
import QuestionHireEventDetails from './QuestionHireEventDetails';
import QuestionHireLocal from './QuestionHireLocal';
import QuestionHireDateTime from './QuestionHireDateTime';
import QuestionHireQtyInterpretes from './QuestionHireQtyInterpretes';
import QuestionHireLogin from './QuestionHireLogin';
import ListaInterpretes from './ListaInterpretes';
import Question2 from './Question2';
import ProcessNewUser from './ProcessNewUser';
import AccountDashboard from './AccountDashboard';
import EventDetails from './EventDetails';
import OrderConfirmation from './OrderConfirmation';
import AccountDashboardContratante from './AccountDashboardContratante'
import LoginGlobal from './LoginGlobal';
import QuestionInterpreteValorHora from './QuestionInterpreteValorHora';

export const screens: Record<string, React.ComponentType<any>> = {
    home: Home,
  questionInterpreteValorHora: QuestionInterpreteValorHora,
  loginGlobal: LoginGlobal,
  question1: Question1,
  eventDetails: EventDetails,
  accountDashboard: AccountDashboard,
  accountDashboardContratante: AccountDashboardContratante,
  questionHireType: QuestionHireType,
  questionHireLogin: QuestionHireLogin,
  questionHireEventDetails: QuestionHireEventDetails,
  questionHireLocal: QuestionHireLocal,
  questionHireDateTime: QuestionHireDateTime,
  questionHireQtyInterpretes: QuestionHireQtyInterpretes,
  listaInterpretes: ListaInterpretes,
  orderConfirmation: OrderConfirmation,
  question2: Question2,
  processNewUser: ProcessNewUser
};

export default function ApplicationHome({ navigation, props }) {
  const { colors: { background } } = useTheme();
  
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.contentAbove}>
          <View>
            <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
            <View>
              <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
            </View>
          </View>
          <View style={[styles.container, { backgroundColor: background }]}>
            <View>
              <Button 
                {...props}
                onPress={() => navigation.navigate('home', { newContratante: null, local: null, hireDateTimeConfig: null, qtyInterpretes: null, selectedInterpretes: null })} 
                style={{marginTop:10, marginBottom:10, padding:30}} 
                labelStyle={{fontSize:13}}
                mode="contained"
              >
                Começar
              </Button>
            </View>
          </View>
        </View>
      </ScrollView>
      <View>
        <Text style={{padding:15,textAlign:'center'}}>
          <Button {...props} onPress={() => navigation.navigate('loginGlobal')}>
            <Text style={{textDecorationLine:'underline'}}>ENTRAR NA MINHA CONTA</Text>
          </Button>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  contentAbove: {
    position:'absolute',
    top:'10%',
    left:0,
    width:'100%',
    zIndex:2000,
    alignContent:'center',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  video: {
    alignSelf: 'center',
    width: '130%',
    height: '130%',
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});