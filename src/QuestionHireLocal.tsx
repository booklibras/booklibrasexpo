import React, { useState } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { TextInput, List, Caption, useTheme, Title, Button, Switch, Text } from 'react-native-paper'
import { ConfirmAddress } from './Examples/Dialogs'

export default function QuestionHireLocal({ route, navigation, props }) {
  const { colors: { background } } = useTheme()
  const newContratante = route.params.newContratante
  const hireDateTimeConfig = route.params.hireDateTimeConfig
  const [ endereco, setEndereco ] = useState('')
  const [ enderecoNumero, setEnderecoNumero ] = useState('')
  const [ address, selectAddress ] = useState(null)
  const [ visible, setVisible ] = useState({})

  const _getVisible = (name: string) => !!visible[name]

  const _toggleDialog = (name: string) => () => {
    if (visible && address) {
      navigation.navigate('questionHireQtyInterpretes', { local: { address, enderecoNumero, isRemoto }, newContratante, hireDateTimeConfig  })
    } 
    return setVisible({ ...visible, [name]: !visible[name] })
  }

  const [linkConferencia, setLinkConferencia] = useState('')
  const [isRemoto, setIsRemoto] = React.useState(false)

  const onToggleSwitch = () => setIsRemoto(!isRemoto)

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:50}}>Localização</Title>
      </View>
      
      <View style={{margin:20,flexDirection:'row', justifyContent:'space-between'}}>
        <View style={{width:'60%', flexDirection:'row'}}>
          <List.Icon style={{margin:0,height:20,width:22, marginRight:10}} icon="link" />
          <Text {...props}>Remoto (On-line)</Text>
        </View>
        <View style={{width:'30%'}}>
        <Switch {...props} value={isRemoto} onValueChange={onToggleSwitch} />
        </View>
      </View>

      {isRemoto ? (
        <View>
          <View style={styles.checkboxContainer}>
            <TextInput
              {...props}
              label="Link da Videoconferência"
              mode="outlined"
              value={linkConferencia}
              onChangeText={linkConferencia => setLinkConferencia(linkConferencia)}
              style={{width:340}}
            />
          </View>
          <View style={{marginTop:20}}>
            <Button 
              {...props}
              style={{
                marginTop:10, 
                marginBottom:10, 
                paddingLeft:50,
                paddingRight:50,
                paddingTop:20,
                paddingBottom:20,
              }} 
              disabled={(!linkConferencia || linkConferencia === '')}
              onPress={() => navigation.navigate('questionHireQtyInterpretes', { local: { linkConferencia }, newContratante, hireDateTimeConfig })}
              labelStyle={{fontSize:16}}  
              mode="contained"
            >
              Continuar
            </Button>
          </View>
        </View>
      ) : (
        <View>
          <View style={styles.checkboxContainer}>
            <TextInput
              {...props}
              label="Digite o CEP ou o nome da rua"
              mode="outlined"
              value={endereco}
              onChangeText={endereco => setEndereco(endereco)}
              style={{width:340}}
            />
          </View>
          <View style={styles.checkboxContainer}>
            <TextInput
              {...props}
              label="Número / Apto / Bloco / Ref"
              mode="outlined"
              value={enderecoNumero}
              onChangeText={enderecoNumero => setEnderecoNumero(enderecoNumero)}
              style={{width:340}}
            />
          </View>
          <View style={{marginTop:20}}>
            <Button 
              {...props}
              style={{
                marginTop:10, 
                marginBottom:10, 
                paddingLeft:50,
                paddingRight:50,
                paddingTop:20,
                paddingBottom:20,
              }} 
              disabled={(!endereco || endereco === '') || (!enderecoNumero || enderecoNumero === '')}
              onPress={_toggleDialog('confirmAddressPopup')} 
              labelStyle={{fontSize:16}}  
              mode="contained"
            >
              Continuar
            </Button>
            <ConfirmAddress
                {...props}
                props={props}
                address={address}
                selectAddress={selectAddress}
                visible={_getVisible('confirmAddressPopup')}
                close={_toggleDialog('confirmAddressPopup')}
              />
          </View>
        </View>
      )}
      
      
    </View>
  )
}

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
})