import React, { useState } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { TextInput, Caption, useTheme, Title, Button, Text } from 'react-native-paper'

export default function QuestionInterpreteValorHora({ route, navigation, props }) {
  const CEP = route.params.CEP
  const userData = route.params.userData
  const { colors: { background } } = useTheme()
  const [ valorHora, setValorHora ] = useState(100)

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:50}}>Quanto você cobra por hora?</Title>
      </View>
      <View style={{...styles.checkboxContainer, alignItems:'center'}}>
        <Text style={{fontSize:20,marginRight:20}} {...props}>R$</Text>
        <TextInput
          {...props}
          label=""
          placeholder="100"
          mode="outlined"
          value={valorHora}
          onChangeText={valor => setValorHora(parseFloat(valor))}
          style={{width:100}}
        />
      </View>
      {/* {valorHora < 50 && (
        <Text {...props} style={{textAlign:'center', color: 'red'}}>Valor mínimo R$ 50</Text>
      )} */}
      <View style={{marginTop:40}}>
        <Button 
          {...props}
          style={{
            marginTop:10, 
            marginBottom:10, 
            paddingLeft:50,
            paddingRight:50,
            paddingTop:20,
            paddingBottom:20,
          }} 
          disabled={(!valorHora || valorHora === 0)}
          // onPress={() => navigation.navigate('questionInterpreteValorHora', { selectedTipoDeDeslocamento, userData })} 
          onPress={() => navigation.navigate('processNewUser', { processGo: true, valorHora, CEP, userData })}
          // onPress={() => navigation.navigate('question3', { userData, selectedTipoDeDeslocamento, valorHora })} 
          labelStyle={{fontSize:16}}  
          mode="contained"
        >
          Continuar
        </Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
})