import * as React from 'react';
import { ScrollView, View, StyleSheet, Platform, Button } from 'react-native';
import {
  Drawer,
  Switch,
  TouchableRipple,
  Text,
  Colors,
  useTheme,
} from 'react-native-paper';

type Props = {
  toggleTheme: () => void;
  toggleRTL: () => void;
  isRTL: boolean;
  isDarkTheme: boolean;
};

export default function DrawerItems({ toggleTheme, toggleRTL, isRTL, isDarkTheme }) {

  const DrawerItemsData = [  
    { label: 'Perfil', icon: 'face', key: 0 },
    { label: 'Trabalhos', icon: 'briefcase', key: 1 },
    { label: 'Pagamentos', icon: 'wallet', key: 2 },
    { label: 'Deslocamento', icon: 'car', key: 3 },
    { label: 'Suporte', icon: 'comment-question', key: 4 },
    { label: 'Logout', icon: 'export', key: 5 },
  ];
  
  // const DrawerItems = ({ toggleTheme, toggleRTL, isRTL, isDarkTheme }: Props) => {
    const [drawerItemIndex, setDrawerItemIndex] = React.useState<number>(0);
  
    const _setDrawerItem = (index: number) => setDrawerItemIndex(index);
  
    const { colors } = useTheme();
  
    return (
      <ScrollView style={[styles.drawerContent, { backgroundColor: colors.surface }]}>
        <Drawer.Section>
          {DrawerItemsData.map((props, index) => (
            <Drawer.Item
              {...props}
              key={props.key}
              theme={
                props.key === '' //todo check
                  ? { colors: { primary: Colors.tealA200 } }
                  : undefined
              }
              // active={drawerItemIndex === index}
              onPress={() => {
                _setDrawerItem(index)
              }}
            />
          ))}
        </Drawer.Section>
  
        <Drawer.Section>
          <TouchableRipple onPress={toggleTheme}>
            <View style={styles.preference}>
              <Text>Modo escuro</Text>
              <View pointerEvents="none">
                <Switch value={isDarkTheme} />
              </View>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </ScrollView>
    );
  // };
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    paddingTop: 50
    // paddingTop: Platform.OS === 'android' ? 25 : 22,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});