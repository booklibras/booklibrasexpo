import * as React from 'react';
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native';
import { useTheme, Button, Caption } from 'react-native-paper';

export default function ApplicationHome({ navigation, props }) {
  const { colors: { background } } = useTheme();
  
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.contentAbove}>
        <View>
          <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
          <View>
            <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
          </View>
        </View>
        <View style={[styles.container, { backgroundColor: background }]}>
          <View>
            <Button 
              {...props}
              icon="camera"
              onPress={() => navigation.navigate('questionHireType', { 
                newContratante: null, 
                local: null, 
                hireDateTimeConfig: null, 
                qtyInterpretes: null, 
                selectedInterpretes: null 
              })} 
              style={{marginTop:10, marginBottom:10, padding:30}} 
              labelStyle={{fontSize:13}}
              mode="contained"
            >
              Procuro Intérpretes
            </Button>
            <Button 
              {...props}
              icon="camera"  
              onPress={() => navigation.navigate('question1', {})}
              style={{marginTop:10, marginBottom:10, padding:30}}
              labelStyle={{fontSize:13}}
              mode="contained"
            >
              Sou Intérprete
            </Button>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  contentAbove: {
    position:'absolute',
    top:'10%',
    left:0,
    width:'100%',
    zIndex:2000,
    alignContent:'center',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  video: {
    alignSelf: 'center',
    width: '130%',
    height: '130%',
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});