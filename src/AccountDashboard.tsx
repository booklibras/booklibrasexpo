import React, { useEffect, useState } from 'react'
import { View, ScrollView, StyleSheet, Text, RefreshControl } from 'react-native'
import { Divider, TouchableRipple, ActivityIndicator, Avatar, List, useTheme, Title, Button } from 'react-native-paper'

import * as Notifications from 'expo-notifications';

export default function AccountDashboard({ route, navigation, props }) {
  const [ token, setToken ] = useState(null)
  const [ userData, setUserData ] = useState(route.params.accountData)
  const [ isLoaded, setIsLoaded ] = useState(false)
  const { colors: { background } } = useTheme()
  const [ sendingDecision, setSendingDecision ] = useState(null)

  const postServiceAcceptance = (trabalho, servico, decision) => () => {
    setSendingDecision(servico.Id)
    fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasExponentPushTokenAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
        decision,
        trabalhoId: trabalho.Id, 
        servicoId: servico.Id, 
        contratanteId: trabalho.Contratante.Id 
      })
    }).then(resp => resp.json()).then(contratanteToken => {
      fetch('https://api.expo.dev/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify([
          {
            to: contratanteToken,
            title: trabalho.Name,
            body: userData.Name + ' ' + (decision === 'accept' ? 'aceitou' : 'recusou') + ' o trabalho'
          }
        ])
      }).then(() => {
        retrieveUserData().then(() => setSendingDecision(null))
      })
    }).catch(err => {
      console.log(err)
    })
  }

  const retrieveUserData = () => {
    console.log('retrieved')
    console.log(userData)
    return fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasInterpreteAPI?interpreteSocialMediaId=' + userData['Social_Media_ID__c'])
        .then(response => response.json()).then(responseJson => {
          const interprete = JSON.parse(responseJson)
          if (interprete && interprete.Id) {
            setUserData(interprete) 
            setIsLoaded(true)
          } else {
            navigation.navigate('Home', { userData: null, accountData: null })
          }
        }).catch((error) => {
          console.error(error)
        })
  }

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    return retrieveUserData().then(() => setRefreshing(false));
  }, []);
  
  const fixedDate = (dateStr) => {
    if (!dateStr) return dateStr
    const dateArr = dateStr.split('-')
    return dateArr[2] + ' ' + monthMap[dateArr[1]] + ' ' + dateArr[0]
  }

  useEffect(() => {
    registerForPushNotification().then(tokenRes => {
      setToken(tokenRes)
      fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasExponentPushTokenAPI', {
        method: 'PATCH',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ token: tokenRes, userData, type: 'interprete' })
      }).then(resp => resp.json()).then(res => {
        setUserData({...userData, ExponentPushToken__c: tokenRes})
      })
    })
    if (userData['register']) {
      retrieveUserData()
    }
    if (userData['exists']) {
      setIsLoaded(true)
    }
  }, [])

  async function registerForPushNotification(){
    const { status } = await Notifications.requestPermissionsAsync()
    if (status !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    let token = (await Notifications.getExpoPushTokenAsync()).data;
    return token
  }

  const monthMap = {
    '01': 'JAN',
    '02': 'FEV',
    '03': 'MAR',
    '04': 'ABR',
    '05': 'MAI',
    '06': 'JUN',
    '07': 'JUL',
    '08': 'AGO',
    '09': 'SET',
    '10': 'OUT',
    '11': 'NOV',
    '12': 'DEZ',
  }

  return (
    <View style={{flex:1}}>
      <ScrollView  
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      >
        {userData && isLoaded ? 
          (
            <View style={styles.container}>
              <View style={{marginBottom:10}}>
                <Avatar.Image {...props} size={130} source={{uri: userData['Photo_URL__c']}} />
              </View>
              <View>
                {!userData['Reputacao__c'] || userData['Reputacao__c'] === 0 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {userData['Reputacao__c'] === 1 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {userData['Reputacao__c'] === 2 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {userData['Reputacao__c'] === 3 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {userData['Reputacao__c'] === 4 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star-outline" />
                  </View>
                ) : null}
                {userData['Reputacao__c'] === 5 ? (
                  <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                    <List.Icon style={{margin:0,height:25,width:25}} color="orange" icon="star" />
                  </View>
                ) : null}
                <Title style={{textAlign:'center'}}>Olá, {userData['Name'].split(' ')[0]}!</Title>
                {/* <Text style={{textAlign:'center', fontStyle: 'italic'}}>{userData['Score__c']} pontos</Text> */}
              </View>
              <View>
                <View style={{marginTop:30,flexDirection:'row', justifyContent:'space-between'}}>
                  <View style={{width:'40%',flexDirection:'row'}}>
                    <List.Icon style={{margin:0,height:25,width:25, marginRight:5}} icon="wallet" />
                    <Text style={{fontSize:15, fontWeight:'bold'}}>Saldo atual</Text>
                  </View>
                  <View style={{width:'40%'}}>
                   <Text style={{textAlign:'right',fontSize:15,fontWeight:'bold', color:'darkgreen'}}>R$ {userData['Saldo__c']}</Text>
                  </View>
                </View>
                  {userData['Proxima_Transferencia__c'] && (
                    <View style={{marginTop:20,flexDirection:'row', justifyContent:'space-between'}}>
                      <View style={{width:'60%', flexDirection:'row'}}>
                        <List.Icon style={{margin:0,height:20,width:22, marginRight:5}} icon="calendar-check" />
                        <Text style={{fontStyle:'italic', fontSize:13}}>Próxima transferência</Text>
                      </View>
                      <View style={{width:'30%'}}>
                        <Text style={{fontStyle:'italic', fontSize:13, textAlign:'right'}}>{fixedDate(userData['Proxima_Transferencia__c'])}</Text>
                      </View>
                    </View>
                  )}
                <Divider {...props} style={{marginTop:30,marginBottom:30}} />

                <View style={{marginBottom:10}}>
                  <Text style={{fontSize:17, fontWeight:'bold',}}>PRÓXIMOS TRABALHOS</Text>
                </View>

                {userData['Trabalhos'].length === 0 ? ( <Text>Nada agendado</Text>) : null}

                {userData['Trabalhos'].map(trabalho => (
                  trabalho.servicos.map((servico, sindex) => (
                    <View key={sindex}>
                    {sendingDecision === servico.Id ? (
                      <View style={{backgroundColor:'#ecf0f1', padding:50}}>
                        <ActivityIndicator {...props} />
                        <Text style={{textAlign:'center', margin:10, fontSize:11}}>CARREGANDO</Text>
                      </View>
                      ) : (
                        <View>
                        <TouchableRipple
                          {...props}
                          style={{backgroundColor: servico.StatusBG__c.split('|')[0], paddingBottom:10}}
                          onPress={() => navigation.navigate('eventDetails', { userData, trabalho, servico })}
                          rippleColor="rgba(0, 0, 0, .32)"
                        >
                          <View>
                            <View style={{marginLeft:15,marginTop:20, flexDirection:'row', justifyContent:'space-between'}}>
                              <View style={{width:'50%'}}>

                                <Text style={{fontSize:12,marginBottom:5}}>{servico.Status__c}</Text>

                                <View style={{flexDirection:'row'}}>
                                  <List.Icon style={{margin:0,height:25,width:25, marginRight:5}} icon="calendar" />
                                  <View>
                                    <Text style={{fontSize:15, fontWeight:'bold'}}>{trabalho.Name}</Text>
                                  </View>
                                </View>
                                
                                <View style={{flexDirection:'row'}}>
                                  <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="clock" />
                                  <View>
                                    <Text style={{fontSize:15,marginTop:10,marginBottom:10}}>{servico.Hora_Inicio__c.split(':00.000Z')[0]} - {servico.Hora_Fim__c.split(':00.000Z')[0]}</Text>
                                  </View>
                                </View>
                              </View>
                              <View style={{width:'20%'}}>
                                <View>
                                  <Text style={{textAlign:'center'}}>{monthMap[servico.Data__c.split('-')[1]]}</Text>
                                  <Text style={{textAlign:'center', fontSize:30, fontWeight:'bold'}}>{servico.Data__c.split('-')[2]}</Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        </TouchableRipple>
                        <View style={{marginBottom:10, paddingTop:10,backgroundColor:servico.StatusBG__c.split('|')[1]}}>
                          {servico.Status__c === 'Agendado' && (
                            <View style={{marginBottom:10, flexDirection:'row', alignItems: 'center', alignContent:'center', justifyContent:'center'}}>
                              <List.Icon style={{marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="check" />
                              <View>
                                <Text style={{fontSize:15,marginTop:10,marginBottom:10}}>Você aceitou esse trabalho</Text>
                              </View>
                            </View>
                          )}
                          {servico.Status__c === 'Aberto' && (
                            <View style={{marginBottom:10}}>
                              <Button 
                                {...props} 
                                icon="check" 
                                onPress={postServiceAcceptance(trabalho, servico, 'accept')}
                                style={{padding:5,marginLeft:20,marginRight:20}} 
                                mode="contained"
                              >
                                Aceitar trabalho
                              </Button>
                              <Button 
                                {...props}  
                                onPress={() => {}}
                              >
                                <Text style={{textDecorationLine:'underline'}}>Recusar</Text>
                              </Button>
                            </View>
                          )}
                        </View>
                        </View>    
                      )}
                      <Divider {...props} />
                    </View>
                  ))
                ))}
              </View>
            </View>
          ) : (
            <View style={{flex:1}}>
              <ActivityIndicator {...props} animating={true} size="large" />
            </View>
          )
        }
      </ScrollView>
      <Button 
        {...props}
        icon="export"  
        onPress={() => navigation.navigate('welcome', {})}
        style={{marginBottom:10, padding:10}} 
        labelStyle={{fontSize:13}}
      >
        LOGOUT
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:50,
    height:50
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});