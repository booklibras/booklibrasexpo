import React, { useEffect, useState } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { TextInput, Caption, useTheme, Title, Button, Text } from 'react-native-paper'

export default function QuestionHireQtyInterpretes({ route, navigation, props }) {
  const newContratante = route.params.newContratante
  const hireDateTimeConfig = route.params.hireDateTimeConfig
  const local = route.params.local
  const { colors: { background } } = useTheme();
  const [ qtyInterpretes, setQtyInterpretes ] = useState()

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View >
        <Title style={{textAlign:'center', marginBottom:40}}>QUANTOS INTÉRPRETES DE LIBRAS SERÃO NECESSÁRIOS?*</Title>
      </View>
      <View style={styles.checkboxContainer}>
        <TextInput
          {...props}
          keyboardType="number-pad"
          placeholder="1"
          mode="outlined"
          value={qtyInterpretes}
          onChangeText={setQtyInterpretes}
          style={{width:100, fontSize:25}}
        />
      </View>
      <View style={{marginLeft:20, marginRight:20}}>
        <Text {...props} style={{textAlign: 'center', fontStyle:'italic'}}>*É de grande importância que pelo menos 2 Intérpretes realizem o serviço para trabalhos cuja duração ultrapassa uma hora.</Text>
      </View>
      <View style={{marginTop:40}}>
        <Button 
          {...props}
          style={{
            marginTop:10, 
            marginBottom:10, 
            paddingLeft:50,
            paddingRight:50,
            paddingTop:20,
            paddingBottom:20,
          }} 
          disabled={qtyInterpretes == '0'}
          onPress={() => navigation.navigate('questionHireLogin', { newContratante, local, hireDateTimeConfig, qtyInterpretes })} 
          labelStyle={{fontSize:16}}  
          mode="contained"
        >
          SELECIONAR INTÉRPRETE{parseInt(qtyInterpretes) > 1 ? 'S' : ''}
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});