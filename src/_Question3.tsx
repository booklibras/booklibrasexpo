// import React, { useState } from 'react'
// import { View, StyleSheet, Image } from 'react-native';
// import { TextInput, Caption, useTheme, Title, Button } from 'react-native-paper'
// // import { ConfirmAddress } from './Examples/Dialogs';

// export default function Question3({ route, navigation, props }) {
//   const { colors: { background } } = useTheme();
//   const userData = route.params.userData
//   const selectedTipoDeDeslocamento = route.params.selectedTipoDeDeslocamento
//   const valorHora = route.params.valorHora
//   // const [ endereco, setEndereco ] = useState('');
//   // const [ enderecoNumero, setEnderecoNumero ] = useState('');
//   // const [ address, selectAddress ] = useState(null)
//   // const [ visible, setVisible ] = useState({});

//   // const _getVisible = (name: string) => !!visible[name];

//   // const _toggleDialog = (name: string) => () => {
//   //   if (visible && address) {
//   //     navigation.navigate('processNewUser', { processGo: true, valorHora, address, enderecoNumero, userData, selectedTipoDeDeslocamento })
//   //   } 
//   //   return setVisible({ ...visible, [name]: !visible[name] })
//   // };

//   return (
//     <View style={styles.container}>
//       <View style={{marginBottom:50}}>
//         <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
//         <View>
//           <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
//         </View>
//       </View>
//       <View>
//         <Title style={{textAlign:'center', marginBottom:50}}>Qual é o valor da sua hora de trabalho?</Title>
//       </View>
//       <View style={styles.checkboxContainer}>
//         <TextInput
//           {...props}
//           label="Endereço"
//           mode="outlined"
//           value={endereco}
//           onChangeText={endereco => setEndereco(endereco)}
//           style={{width:340}}
//         />
//       </View>
//       <View style={{marginTop:40}}>
//         <Button 
//           {...props}
//           style={{
//             marginTop:10, 
//             marginBottom:10, 
//             paddingLeft:50,
//             paddingRight:50,
//             paddingTop:20,
//             paddingBottom:20,
//           }} 
//           disabled={(!endereco || endereco === '') || (!enderecoNumero || enderecoNumero === '')}
//           // onPress={_toggleDialog('confirmAddressPopup')} 
  //           onPress={() => navigation.navigate('processNewUser', { processGo: true, valorHora, address, enderecoNumero, userData, selectedTipoDeDeslocamento })}
//           labelStyle={{fontSize:16}}  
//           mode="contained"
//         >
//           Continuar
//         </Button>
//         {/* <ConfirmAddress
//             {...props}
//             props={props}
//             address={address}
//             selectAddress={selectAddress}
//             visible={_getVisible('confirmAddressPopup')}
//             close={_toggleDialog('confirmAddressPopup')}
//           /> */}
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   photo: {
//     width:100,
//     height:100
//   },
//   container: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   checkboxContainer: {
//     flexDirection: "row",
//     marginBottom: 10,
//   },
//   checkbox: {
//     alignSelf: "center",
//   },
//   label: {
//     margin: 8,
//   },
// });