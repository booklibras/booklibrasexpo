import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Caption, useTheme, Title, Button } from 'react-native-paper'
import FacebookLogin from './FacebookLogin';

export default function QuestionHireLogin({ route, navigation, props }) {
  const newContratante = route.params.newContratante
  const local = route.params.local
  const hireDateTimeConfig = route.params.hireDateTimeConfig
  const qtyInterpretes = route.params.qtyInterpretes

  const [ contratanteData, setContratanteData ] = useState({});
  const { colors: { background } } = useTheme();

  useEffect(() => {
    if (contratanteData['register']) {
      navigation.navigate('listaInterpretes', {
        local, hireDateTimeConfig, qtyInterpretes, 
        newContratante: {
          type: newContratante.type,
          id: contratanteData['id'],
          name: contratanteData['name'],
          email: contratanteData['email'],
          profile_img: contratanteData['picture']['data']['url'],
          sfId: contratanteData['sfId']
        } 
      })
    }
    if (contratanteData['exists']) {
      navigation.navigate('listaInterpretes', { local, hireDateTimeConfig, qtyInterpretes, newContratante: contratanteData })
    }
  })

  return (
    <View style={styles.container}>
      <View style={{marginBottom:50}}>
        <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
        <View>
          <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
        </View>
      </View>
      <View>
        <Title style={{textAlign:'center', marginBottom:50}}>Identifique-se para continuar</Title>
      </View>
      <View style={styles.checkboxContainer}>
        <FacebookLogin {...props} isContratante={true} setContratanteData={setContratanteData} />
        <Button 
          {...props} 
          icon="google" 
          onPress={() => navigation.navigate('question3')}
          style={{marginTop:10, marginBottom:10, padding:30}} 
          mode="contained"
        >
          Entrar com Google
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});
