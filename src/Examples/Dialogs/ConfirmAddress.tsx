import * as React from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { Dimensions, ScrollView, Text, View } from 'react-native';
import { Button, List, Portal, Dialog, ActivityIndicator, TouchableRipple } from 'react-native-paper';

export default function ConfirmAddress ({ props, visible, close, address, selectAddress }) {
  
  const [ isConfirming, setIsConfirming ] = React.useState(false)
  
  return (

    <Portal>
      <Dialog
        onDismiss={close}
        visible={visible}
        style={{ maxHeight: 0.9 * Dimensions.get('window').height }}
      >
        <Dialog.Title {...props}>
          <View style={{flex:1, flexDirection:'row'}}>
            <List.Icon style={{height:30, width: 30, margin:0}} icon="map-marker" />
            <Text style={{color:'#000', fontWeight:'bold', fontSize:20, marginLeft:10}}>CONFIRME O ENDEREÇO</Text>
          </View>
        </Dialog.Title>
        
        <Dialog.ScrollArea style={{ paddingHorizontal: 0 }}>
          {!isConfirming ? 
            (
              <ScrollView contentContainerStyle={{ paddingBottom:20 }}>
                <TouchableRipple {...props} style={{backgroundColor: address === 'abc123' ? 'rgba(0,0,0,.32)' : 'transparent', padding:15}} onPress={() => selectAddress('abc123')} rippleColor="rgba(0, 0, 0, .32)">
                  <Text style={{fontWeight: address === 'abc123' ? 'bold' : 'normal'}}>aaRua João Coelho de Moraes - 03203060 - Vila Bela - São Paulo - SP</Text>
                </TouchableRipple>
                <TouchableRipple {...props} style={{backgroundColor: address === 'def456' ? 'rgba(0,0,0,.32)' : 'transparent', padding:15}} onPress={() => selectAddress('def456')} rippleColor="rgba(0, 0, 0, .32)">
                  <Text style={{fontWeight: address === 'def456' ? 'bold' : 'normal'}}>aaRua João Coelho de Moraes - 03203060 - Vila Bela - São Paulo - SP</Text>
                </TouchableRipple>
                <TouchableRipple {...props} style={{backgroundColor: address === 'ghi678' ? 'rgba(0,0,0,.32)' : 'transparent', padding:15}} onPress={() => selectAddress('ghi678')} rippleColor="rgba(0, 0, 0, .32)">
                  <Text style={{fontWeight: address === 'ghi678' ? 'bold' : 'normal'}}>aaRua João Coelho de Moraes - 03203060 - Vila Bela - São Paulo - SP</Text>
                </TouchableRipple>
              </ScrollView>
            ) : (
              <ActivityIndicator {...props} style={{margin:30}} animating={true} size="large" />
            )
          }
        </Dialog.ScrollArea>
        <Dialog.Actions>
          <Text style={{paddingTop:10,textAlign:'center', width:'100%'}}>
            <Button {...props} mode="contained"
              icon="map-marker"
              disabled={isConfirming || address === null}
              style={{marginBottom:50, padding:10}} 
              onPress={close}
            >
              {isConfirming ? 'CARREGANDO...' : 'CONFIRMAR ENDEREÇO'}
            </Button>

            {'\n'}
            
            {!isConfirming ? 
              (
                <Button style={{padding:10}} onPress={close}>
                  <Text style={{textDecorationLine:'underline'}}>VOLTAR</Text>
                </Button>
              ) : null
            }
          </Text>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  )
}
