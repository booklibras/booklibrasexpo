import * as React from 'react'
import { Dimensions, ScrollView, Text, View, Linking, TextInput } from 'react-native'
import { Paragraph, Button, List, Portal, Dialog, Checkbox, ActivityIndicator } from 'react-native-paper'

export default function CancelInvitedEvent ({
  visible,
  close,
}: {
  visible: boolean;
  close: () => void;
}) {
  const [reason, onChangeReason] = React.useState(null)
  const [isCancelling, setIsCancelling] = React.useState(false)

  const confirmCancel = () => {
    setIsCancelling(true)
    setTimeout(() => {
      close()
      setIsCancelling(false);
    },5000)
  }
  
  return (

    <Portal>
      <Dialog
        onDismiss={close}
        visible={visible}
        style={{ maxHeight: 0.9 * Dimensions.get('window').height }}
      >
        <Dialog.Title>
          <View style={{flex:1, flexDirection:'row'}}>
            <List.Icon style={{height:30, width: 30, margin:0}} icon="trash-can" />
            <Text style={{color:'#000', fontWeight:'bold', fontSize:20, marginLeft:10}}>CANCELAR TRABALHO</Text>
          </View>
        </Dialog.Title>
        
        <Dialog.ScrollArea style={{ paddingHorizontal: 0 }}>
          {!isCancelling ? 
            (
              <ScrollView contentContainerStyle={{ paddingBottom:10, paddingTop:10, paddingHorizontal: 24 }}>
                <Paragraph>
                  Digite o motivo do cancelamento no campo abaixo:
                </Paragraph>
                <View
                  style={{
                  marginBottom:10,
                  marginTop:10,
                  borderColor: '#000000',
                  borderWidth: 1,
                }}>
                  <TextInput
                    multiline
                    editable={!isCancelling}
                    onChangeText={onChangeReason}
                    value={reason}
                    placeholder="Mínimo 10 caracteres"
                    numberOfLines={4}
                    style={{padding: 10}}
                    maxLength={40}
                  />
                </View>
                <Text style={{fontStyle:'italic'}}>
                  Ao cancelar este trabalho, você confirma que leu e está de acordo com a <Text style={{fontWeight:'bold', color: 'blue', textDecorationLine:'underline'}} onPress={() => Linking.openURL('http://google.com')}>Política de Cancelamento</Text> do BookLibras
                </Text>
              </ScrollView>
            ) : (
              <ActivityIndicator style={{margin:30}} animating={true} size="large" />
            )
          }
        </Dialog.ScrollArea>
        <Dialog.Actions>
        <Text style={{paddingTop:10,textAlign:'center', width:'100%'}}>
          <Button mode="contained"
            icon="trash-can"
            disabled={isCancelling || reason === null || reason.length < 10}
            style={{marginBottom:50, padding:10}} 
            onPress={confirmCancel}
          >
            {isCancelling ? 'CANCELANDO...' : 'CONFIRMAR CANCELAMENTO'}
          </Button>

          {'\n'}
          
          {!isCancelling ? 
            (
              <Button style={{padding:10}} onPress={close}>
                <Text style={{textDecorationLine:'underline'}}>VOLTAR</Text>
              </Button>
            ) : null
          }
        </Text>
      </Dialog.Actions>
    </Dialog>
  </Portal>
  )
}
