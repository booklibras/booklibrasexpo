import * as React from 'react'
import { Dimensions, Text, View } from 'react-native'
import { Avatar, Button, Portal, Dialog, Divider } from 'react-native-paper'

export default function PaymentReview ({ props, visible, close, selectedInterpretes, setGoToPayment, placeOrder }) {

  return (
    <Portal>
      <Dialog
        onDismiss={close}
        visible={visible}
        style={{ maxHeight: 0.9 * Dimensions.get('window').height }}
      >
        <Dialog.Title {...props} style={{textAlign:'center'}}>
          <Text style={{color:'#000', fontWeight:'bold', fontSize:20}}>SUA CONTRATAÇÃO</Text>
        </Dialog.Title>
        
        <Dialog.ScrollArea style={{ paddingHorizontal: 0 }}>
          <View style={{marginLeft:20, marginRight:20, marginBottom:10}}>
            <Text style={{fontWeight:'bold',textAlign:'center', fontSize:16, padding:10}}>Você selecionou</Text>
            <View style={{alignItems:'center', flexDirection:'row', flexWrap:'wrap'}}>
              {selectedInterpretes.map((selectedInterprete, index) => (
                <View key={index} style={{width:'50%', alignItems:'center', marginBottom:30}}>
                  <Avatar.Image {...props} size={50} source={{uri: selectedInterprete.Photo_URL__c}} />
                  <Text style={{fontSize:15, marginTop:5}}>{selectedInterprete.Name}</Text>
                </View>
              ))}
            </View>

            <Text style={{textAlign:'center', fontSize:16, paddingTop:20, fontWeight:'bold', paddingBottom:10}}>Detalhes do Pedido</Text>
            <View style={{marginTop:10,flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{flexDirection:'row'}}>
                <Text style={{fontWeight:'bold',fontSize:15}}>
                  Intérprete de Libras
                  {'\n'}
                  <Text style={{fontWeight:'normal',fontSize:13, fontStyle:'italic'}}>20/10/2021 - 13:00 às 15:00</Text>
                </Text>
              </View>

              <View>
                <Text style={{textAlign:'right',fontSize:15, color:'#000'}}>R$ 300</Text>
              </View>
            </View>
            
            <View style={{marginTop:10,flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{flexDirection:'row'}}>
                <Text style={{fontWeight:'bold', fontSize:15}}>Taxa Serviço 10%</Text>
              </View>
              <View style={{alignContent:'flex-end'}}>
                <Text style={{textAlign:'right',fontSize:15, color:'#000'}}>R$ 30</Text>
              </View>
            </View>
            
            <View style={{marginTop:10,flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{flexDirection:'row'}}>
                <Text style={{fontWeight:'bold', fontSize:15}}>Deslocamento Intérprete{selectedInterpretes.length > 1 ? 's' : ''}</Text>
              </View>
              <View style={{alignContent:'flex-end'}}>
                <Text style={{fontSize:15, color:'#000'}}>R$ 45</Text>
              </View>
            </View>

            <Divider {...props} style={{marginTop:10}}/>
            
            <View style={{marginTop:10,flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{flexDirection:'row'}}>
                <Text style={{fontSize:16, fontWeight:'bold'}}>TOTAL</Text>
              </View>
              <View style={{width:'40%'}}>
                <Text style={{textAlign:'right',fontSize:16, fontWeight:'bold', color:'#000'}}>R$ 375</Text>
              </View>
            </View>
          </View>

        </Dialog.ScrollArea>
        <Dialog.Actions>
          <View style={{alignItems:'center', width: '100%', marginTop:10}}>

            <Text style={{textAlign:'center'}}>
              {/* todo terms of service checkbox */}
              <Button {...props} mode="contained" 
                onPress={() => {
                  placeOrder()
                  // setGoToPayment(true)
                }}
                icon="cart"
                style={{padding:10}}
              >
                PROSSEGUIR AO PAGAMENTO
              </Button>

              {'\n'}
              
              <Button {...props} onPress={close}>
                <Text style={{textDecorationLine:'underline'}}>VOLTAR</Text>
              </Button>
            </Text>
          </View>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  )
}
