import React, { useState, useEffect } from 'react'
import { LogBox, View, StyleSheet, Image, Text } from 'react-native'
import { Caption, useTheme, Title, RadioButton, Button } from 'react-native-paper'

export default function Question2({ route, navigation, props }) {
  const userData = route.params.userData  
  const { colors: { background } } = useTheme();
  const [ tiposDeDeslocamento, setTiposDeDeslocamento ] = useState([
    {
      id:'public',
      name:'Transporte Público',
      description:'Ônibus, Metrô, Uber, Taxi, etc.'
    },
    {
      id:'private',
      name:'Veículo Particular'
    }
  ])
  const [ selectedTipoDeDeslocamento, setSelectedTipoDeDeslocamento ] = useState(null);
  const [ deslocamentoDefined, setDeslocamentoDefined ] = useState(false);

  useEffect(() => {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
  });
  
  return (
    <View style={{flex:1}}>
      <View style={styles.container}>
        <View style={{marginBottom:50}}>
          <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
          <View>
            <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
          </View>
        </View>
        <View>
          <Title style={{textAlign:'center', marginBottom:50}}>Como pretende se deslocar para os trabalhos presenciais?</Title>
        </View>
        {tiposDeDeslocamento.map((tipoDeDeslocamento, index) => (
          <View key={index}>
            <View style={styles.checkboxContainer}>
              <RadioButton
                value="second"
                status={ tipoDeDeslocamento['id'] === selectedTipoDeDeslocamento ? 'checked' : 'unchecked' }
                onPress={() => {
                  setSelectedTipoDeDeslocamento(tipoDeDeslocamento['id'])
                  setDeslocamentoDefined(true)
                }}
              />
              <Text style={{padding:5, fontSize:18}}>{tipoDeDeslocamento['name']}</Text>
            </View>
            {tipoDeDeslocamento['description'] ? (
              <View style={{marginBottom:50}}>
                <Text>{tipoDeDeslocamento['description']}</Text>
              </View>
            ) : null}
          </View>  
        ))}

        <View style={{marginTop:40}}>
          <Button 
            {...props}
            style={{
              marginTop:10, 
              marginBottom:10, 
              paddingLeft:50,
              paddingRight:50,
              paddingTop:20,
              paddingBottom:20,
            }} 
            disabled={!deslocamentoDefined}
            onPress={() => navigation.navigate('questionInterpreteValorHora', { selectedTipoDeDeslocamento, userData })} 
            labelStyle={{fontSize:16}}  
            mode="contained"
          >
            Continuar
          </Button>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});