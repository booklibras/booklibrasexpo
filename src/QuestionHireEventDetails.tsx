import React, { useState } from 'react'
import { ScrollView, View, StyleSheet, Image } from 'react-native'
import { TextInput, Title, Caption, useTheme, Button, Paragraph } from 'react-native-paper'

export default function QuestionHireEventDetails({ route, navigation, props }) {
  const { colors: { background } } = useTheme();
  const selectedInterpretes = route.params.selectedInterpretes
  const newContratante = route.params.newContratante
  const local = route.params.local
  const hireDateTimeConfig = route.params.hireDateTimeConfig
  
  const transactionObj = {
    Status_Pagamento__c: 'Pendente',
    Total__c: 375,
    Total_Deslocamento__c: 45,
    Total_Servico_Interpretes__c: 300,
    Total_Taxa_Servico__c: 30,
  }
  
  const [ isSubmitting, setIsSubmitting ] = useState(false)
  const [ ocasiao, setOcasiao ] = useState('');
  const [ contexto, setContexto ] = useState('');
  const [ pedidoId, setPedidoId ] = useState(null);
  
  const insertTrabalho = () => {
    return fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasTrabalhoAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        Name: ocasiao,
        Contexto__c: contexto,
        Contratante__c: newContratante.sfId,
        Link__c: local.linkConferencia || '',
        Local__c: local.address || '',
        Local_Numero__c: local.enderecoNumero || '',
        Remoto__c: local.isRemoto === true,
        totalHours: calculateTotalHours(hireDateTimeConfig.timeStart, hireDateTimeConfig.timeEnd),
        selectedInterpretes, hireDateTimeConfig
      })
    })
  }

  const calculateTotalHours = (timeStart, timeEnd) => {
    let totalHours = parseInt(timeEnd.split(':')[0]) - parseInt(timeStart.split(':')[0])
    if (parseInt(timeEnd.split(':')[1]) > 0) {
      totalHours = totalHours + 1
    }
    return totalHours
  }

  const submitEventDetails = () => {
    setIsSubmitting(true)
    insertTrabalho().then(response => response.json()).then(trabalhoId => {
      transactionObj['Trabalho__c'] = trabalhoId
      fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasPedidoAPI', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(transactionObj)
      }).then(res => res.json()).then(pId => {
        setPedidoId(pId)
        fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasPedidoAPI', {
          method: 'PATCH',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            pedidoId,
            transaction_amount: transactionObj['transaction_amount'],
            status: 'approved'
          })
        }).then(res => res.json()).then(() => {
          for (var i = 0; i < selectedInterpretes.length; i++) {
            let interprete = selectedInterpretes[i]
            fetch('https://api.expo.dev/v2/push/send', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify([
                {
                  to: interprete.ExponentPushToken__c,
                  title: newContratante.Name + ' quer te contratar para um serviço',
                  body: ocasiao
                }
              ])
            }).then(() => {
              if (i === selectedInterpretes.length) {
                navigation.navigate('orderConfirmation', { hireDateTimeConfig, contexto, ocasiao, selectedInterpretes, local, newContratante, paymentStatus: 'APROVADO', transactionObj })
              }
            })
          }
        }).catch((error) => {
          console.error(error);
        })
      }).catch((error) => {
        console.error(error);
      })
    }).catch((error) => {
      console.error(error);
    });
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={{marginBottom:20, alignItems:'center'}}>
          <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
          <View>
            <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
          </View>
        </View>

        <View style={{marginLeft:20, marginRight:20}}>
          <Title style={{textAlign:'center', marginBottom:20}}>Preencha os dados de sua contratação</Title>
        </View>

        <View style={{marginLeft:20,marginRight:20}}>
          <View style={{marginBottom:10}}>
            <View style={styles.checkboxContainer}>
              <TextInput
                {...props}
                label="Tipo de evento ou ocasião"
                mode="outlined"
                value={ocasiao}
                disabled={isSubmitting}
                onChangeText={ocasiao => setOcasiao(ocasiao)}
                style={{width:'100%'}}
              />
            </View>
          </View>

          <View>
            <View>
              <Paragraph style={{textAlign:'center', fontSize:18}}>Contexto</Paragraph>
            </View>
            <View
              style={{
              marginBottom:10,
              marginTop:10,
            }}>
              <TextInput
                {...props}
                multiline
                disabled={isSubmitting}
                editable={true}
                mode="outlined"
                onChangeText={setContexto}
                value={contexto}
                placeholder="Qual será o tema abordado?"
                numberOfLines={10}
              />
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={{padding:20}}>
        <Button 
          {...props}
          style={{
            marginTop:10, 
            marginBottom:10, 
            paddingLeft:50,
            paddingRight:50,
            paddingTop:20,
            paddingBottom:20,
          }} 
          disabled={isSubmitting || (!ocasiao || ocasiao === '') || (!contexto || contexto === '')}
          onPress={() => submitEventDetails()} 
          labelStyle={{fontSize:15}}  
          mode="contained"
        >
          FINALIZAR
        </Button>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});