import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { List, Chip, Caption, useTheme, Text, Button, Title } from 'react-native-paper'

export default function OrderConfirmation({ route, navigation, props }) {
  const { colors: { background } } = useTheme();
  const newContratante = route.params.newContratante
  const paymentStatus = route.params.paymentStatus
  const ocasiao = route.params.ocasiao
  const contexto = route.params.contexto

  // const { colors: { background } } = useTheme();
  const selectedInterpretes = route.params.selectedInterpretes
  // const newContratante = route.params.newContratante
  const local = route.params.local
  const hireDateTimeConfig = route.params.hireDateTimeConfig
  // const qtyInterpretes = route.params.qtyInterpretes
  // const transactionObj = route.params.transactionObj

  // const [ accountData ] = useState({
  //   Name: newContratante.name,
  //   Social_Media_ID__c: newContratante.id,
  //   Email__c: newContratante.email || '',
  //   Photo_URL__c: newContratante.profile_img,
  //   Auth_Type__c: 'Facebook', //@todo
  // })

  // useEffect(() => {
    // fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasPedidoAPI', {
    //     method: 'PATCH',
    //     headers: {
    //       Accept: 'application/json',
    //       'Content-Type': 'application/json'
    //     },
    //     body: JSON.stringify({
    //       pedidoId,
    //       transaction_amount: transactionObj.transaction_amount,
    //       status: paymentStatus
    //     })
    //   }).then(response => response.json()).then(() => {
    //     setIsLoading(false)
    //   }).catch((error) => {
    //     console.error(error);
    //   });
  // })

  /**
   * 
   * date
   * hour init
   * hour end
   * location type
   * link
   * interpretes
   */

  // if (isLoading) {
  //   return (
  //     <View style={styles.container}>
  //       <Text {...props}>Carregando</Text>
  //     </View>
  //   );
  // } else {
  return (
    <View style={styles.container}>
      <View style={styles.overlay}/>
        <View style={styles.contentAbove}>
          <View>
            <Image style={styles.photo} source={{ uri: 'https://image.flaticon.com/icons/png/512/3498/3498146.png' }} />
              <View>
                <Caption style={{textAlign:'center'}}>BOOKLIBRAS</Caption>
              </View>
          </View>
          <View>
            <Title style={{textAlign:'center', marginLeft:20, marginRight:20, marginBottom:40}}>TUDO CERTO!</Title>
            <View style={{marginBottom:10}}>
              <Text {...props} style={{fontWeight: 'bold'}}>Evento / Ocasião:</Text>
              <Text {...props}>{ocasiao}</Text>
            </View>
            <View style={{marginBottom:10}}>
              <Text {...props} style={{fontWeight: 'bold'}}>Tema abordado:</Text>
              <Text {...props}>{contexto}</Text>
            </View>
            <View style={{marginBottom:10}}>
              <Text {...props} style={{fontWeight: 'bold'}}>Data:</Text>
              <Text {...props}>{hireDateTimeConfig.date}</Text>
            </View>
            <View style={{marginBottom:10}}>
              <Text {...props} style={{fontWeight: 'bold'}}>Horário:</Text>
              <Text {...props}>{hireDateTimeConfig.timeStart} - {hireDateTimeConfig.timeEnd}</Text>
            </View>
            <View style={{marginBottom:10}}>
              <Text {...props} style={{fontWeight: 'bold'}}>Local do evento:</Text>
              <Text {...props}>{local.linkConferencia}</Text>
            </View>

            <View style={{marginLeft:20, marginTop:20}}>
              <Text {...props} style={{fontSize:17, fontWeight:'bold',}}>Intérpretes</Text>
            </View>

            <List.Section
              {...props} 
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingHorizontal: 12
              }}
            >
              {selectedInterpretes.map((selectedInterprete, index) => (
                <View key={index}>
                  <Chip
                    {...props}
                    avatar={
                      <Image source={{uri: selectedInterprete.Photo_URL__c}} />
                    }
                  >
                    {selectedInterprete.Name}
                  </Chip>
                </View>  
              ))}
            </List.Section>
            <View>
              <Button 
                {...props}
                onPress={() => navigation.navigate('accountDashboardContratante', { contratante: newContratante })} 
                style={{marginTop:10, marginBottom:10, padding:10}} 
                labelStyle={{fontSize:20}}
                mode="contained"
              >
                OK
              </Button>
          </View>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  photo: {
    width:100,
    height:100
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  contentAbove: {
    position:'absolute',
    top:'10%',
    left:0,
    width:'100%',
    zIndex:2000,
    alignContent:'center',
    alignItems: 'center'
  },
  overlay: {
    zIndex: 1000,
    width: '100%',
    height: '100%',
    top:0,
    left:0,
    position:'absolute',
    
  },
  video: {
    alignSelf: 'center',
    width: '130%',
    height: '130%',
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
