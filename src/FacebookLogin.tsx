import React from 'react'
import { Button } from 'react-native-paper'
import { StyleSheet, View } from 'react-native'
import * as Facebook from 'expo-facebook'

export default function FacebookLogin({ isContratante, setContratanteData, setUserData, isGlobal, props }) {

  const insertContratante = (data) => {
    return fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasContratanteAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        Name: data.name,
        Social_Media_ID__c: data.id,
        Email__c: data.email || '',
        Photo_URL__c: data.picture.data.url,
        Auth_Type__c: 'Facebook'
      })
    })
  } 

  async function logIn() {
    try {
      const appId = "252442110153927"
      const appName = "BLibras"
      await Facebook.initializeAsync({ appId, appName });

      //@ts-ignore
      const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile", "email"],
      });

      if (type === "success") {
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`);
        const retrievedUserData = await response.json()

        if (isGlobal) {
          fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasExistingUserAPI?userSocialMediaId=' + retrievedUserData['id'])
            .then(response => response.json()).then(resObj => {
              const user = JSON.parse(resObj)
              setUserData({...user, exists: true, sfId: user.Id})
            }).catch((error) => {
              console.error(error)
            });
        } else if (isContratante) {
          fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasContratanteAPI?contratanteSocialMediaId=' + retrievedUserData['id'])
            .then(response => response.json()).then(resObj => {
              const contratante = JSON.parse(resObj)
              if (contratante && contratante.Id) {
                setContratanteData({...contratante, exists: true, sfId: contratante.Id})
              } else {
                insertContratante(retrievedUserData).then(response => response.json()).then(sfId => {
                  setContratanteData({...retrievedUserData, register: true, sfId})
                }).catch((error) => {
                  console.error(error);
                });
              }
            }).catch((error) => {
              console.error(error)
            });
        } else {
          fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasInterpreteAPI?interpreteSocialMediaId=' + retrievedUserData['id'])
            .then(response => response.json()).then(resObj => {
              
              const interprete = JSON.parse(resObj)
              if (interprete && interprete.Id) {
                setUserData({...interprete, exists: true})
              } else {
                setUserData({...retrievedUserData, register: true})
              }
            }).catch((error) => {
              console.error(error)
            });
        }
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`)
    }
  }

  return (
    <View>
      <Button 
        {...props}
        icon="facebook"  
        onPress={() => logIn()} 
        style={{marginTop:10, marginBottom:10, padding:30}} 
        labelStyle={{fontSize:13}}
        mode="contained"
      >
        Entrar com Facebook
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  name: {
    textAlign: "center",
    marginTop: 8,
  },
});