import React, { useEffect, useState } from 'react'
import { View, ScrollView, StyleSheet, Text } from 'react-native'
import { Divider, Button, TouchableRipple, ActivityIndicator, Avatar, List, Title, useTheme } from 'react-native-paper'
import * as Notifications from 'expo-notifications'

export default function AccountDashboardContratante({ route, navigation, props }) {
  const contratante = route.params.contratante
  const [ userData, setUserData ] = useState({})
  const [ isLoaded, setIsLoaded ] = useState(false)
  const { colors: { background } } = useTheme()
  const [ token, setToken ] = useState(null)

  const monthMap = {
    '01': 'JAN',
    '02': 'FEV',
    '03': 'MAR',
    '04': 'ABR',
    '05': 'MAI',
    '06': 'JUN',
    '07': 'JUL',
    '08': 'AGO',
    '09': 'SET',
    '10': 'OUT',
    '11': 'NOV',
    '12': 'DEZ',
  }  

  async function registerForPushNotification(){
    const { status } = await Notifications.requestPermissionsAsync()
    if (status !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    let token = (await Notifications.getExpoPushTokenAsync()).data;
    return token
  }

  async function getContratanteData(tokenRes) {
    fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasContratanteAPI?contratanteSocialMediaId=' + contratante['Social_Media_ID__c'])
      .then(response => response.json()).then(responseJson => {
        const contratanteObj = JSON.parse(responseJson)
        if (contratanteObj && contratanteObj.Id) {
          console.log(contratanteObj)
          setUserData({ ...contratanteObj, ExponentPushToken__c: tokenRes})  
          setIsLoaded(true)
        } else {
          navigation.navigate('Home', { userData: null, accountData: null })
        }
      }).catch((error) => {
        console.error(error)
      })
  }

  useEffect(() => {
    registerForPushNotification().then(tokenRes => {
      setToken(tokenRes)

      fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasExponentPushTokenAPI', {
        method: 'PATCH',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
          token: tokenRes, 
          userData: contratante, 
          type: 'contratante' 
        })
      }).then(resp => resp.json()).then(res => {
        getContratanteData(tokenRes)
      })
    })
  }, [])

  const addTrabalho = () => {

    // return fetch('https://booklibras-developer-edition.na163.force.com/services/apexrest/BookLibrasTrabalhoAPI', {
    //   method: 'POST',
    //   headers: {
    //     Accept: 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify({ userData })
    // }).then(resRaw => resRaw.json()).then(res => {
      
    // }).catch((error) => {
    //   console.error(error)
    // })
  }

  return (
    <ScrollView>
      {userData && isLoaded ? 
        (
          <View style={styles.container}>
            <View style={{marginBottom:10}}>
              <Avatar.Image {...props} size={130} source={{uri: userData['Photo_URL__c']}} />
            </View>
            <View>
              <Title style={{textAlign:'center'}}>Olá, {userData['Name'].split(' ')[0]}!</Title>
            </View>
            <View>
              <View style={{marginTop:30,flexDirection:'row', justifyContent:'space-between'}}>
                <View style={{width:'40%',flexDirection:'row'}}>
                  <List.Icon style={{margin:0,height:25,width:25, marginRight:5}} icon="wallet" />
                  <Text style={{fontSize:15, fontWeight:'bold'}}>Contratações</Text>
                </View>
                <View style={{width:'40%'}}>
                  <Text style={{textAlign:'right',fontSize:15,fontWeight:'bold', color:'#000'}}>{userData['Trabalhos'].length || 0}</Text>
                </View>
              </View>
              <Divider {...props} style={{marginTop:30,marginBottom:30}} />
              
              <View>
                <Text style={{fontSize:17, fontWeight:'bold',}}>PRÓXIMOS TRABALHOS</Text>
              </View>

              {userData['Trabalhos'].length && userData['Trabalhos'].map(trabalho => (
                trabalho.servicos.map((servico, sindex) => (
                  <View key={sindex}>
                    <TouchableRipple
                      {...props}
                      onPress={() => navigation.navigate('eventDetails', { trabalho, servico })}
                      rippleColor="rgba(0, 0, 0, .32)"
                    >
                      <View style={{marginLeft:15,marginTop:30, marginBottom:20, flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{width:'50%'}}>

                          <View style={{flexDirection:'row'}}>
                            <List.Icon style={{margin:0,height:25,width:25, marginRight:5}} icon="calendar" />
                            <View>
                              <Text style={{fontSize:15, fontWeight:'bold'}}>{trabalho.Name}</Text>
                            </View>
                          </View>
                          
                          <View style={{flexDirection:'row'}}>
                            <List.Icon style={{marginTop:10, marginBottom:10, marginLeft:0, height:25,width:25, marginRight:5}} icon="clock" />
                            <View>
                              <Text style={{fontSize:15,marginTop:10,marginBottom:10}}>{servico.Hora_Inicio__c.split(':00.000Z')[0]} - {servico.Hora_Fim__c.split(':00.000Z')[0]}</Text>
                            </View>
                          </View>
                        </View>
                        <View style={{width:'20%'}}>
                          <View>
                            <Text style={{textAlign:'center'}}>{monthMap[servico.Data__c.split('-')[1]]}</Text>
                            <Text style={{textAlign:'center', fontSize:30, fontWeight:'bold'}}>{servico.Data__c.split('-')[2]}</Text>
                          </View>
                        </View>
                      </View>
                    </TouchableRipple>
                    <Divider {...props} />
                  </View>
                ))
              )) || (
                <View>
                  <Text style={{textAlign:'center'}}>Nenhum trabalho agendado</Text>
                </View>
              )}
              <Button 
                {...props} 
                icon="plus" 
                onPress={() => navigation.navigate('questionHireType', { 
                  newContratante: userData, 
                  logged: true,
                  local: null, 
                  hireDateTimeConfig: null, 
                  qtyInterpretes: null, 
                  selectedInterpretes: null 
                })} 
                style={{padding:5,marginLeft:20,marginRight:20}} 
                mode="contained"
              >
                Adicionar trabalho
              </Button>
            </View>
          </View>
        ) : (
          <View style={{flex:1}}>
            <ActivityIndicator {...props} animating={true} size="large" />
          </View>
        )
      }
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  photo: {
    width:50,
    height:50
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
});